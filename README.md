# ClsViz

ClsViz is designed to be a browser for table type data. It combines the advantage of various technology such as spectral clustering, model selection, neighborhood embedding and javascript interactive figure to help investigators to get a quick data impression.  

## Features
### Clustering
- Spectral Clustering ( Coordinate Translate Based on Similarity Matrix)
- Integrated Multiple Sources Data
- Automatically select cluster number k
- tSNE for Visualizaiton

### Visualization
- JavaScript Interactive Figure powered by d3js  
- Innovated Contingency Table Visualization 
- Support Different Input Sources: Google Sheet or URL or Local TSV file
- Support Figure PDF Download
- Support URL parameters 


## Demo Website
- [Visit Demo Website](http://garberwiki.umassmed.edu:8000)

## Dependency
### Clustering Program Dependency
- scipy
- numpy
- scikit-learn
- argparse
- csv

### Visualization Program Dependency
- d3js
- jQuery
- librsvg ( for convert svg to pdf )
